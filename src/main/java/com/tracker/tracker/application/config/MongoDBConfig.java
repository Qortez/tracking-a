package com.tracker.tracker.application.config;

import com.tracker.tracker.application.document.Users;
import com.tracker.tracker.application.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackageClasses = UserRepository.class)
@Configuration
public class MongoDBConfig {

    @Bean
    CommandLineRunner commandLineRunner(UserRepository userRepository) {
        return args -> userRepository.save(new Users(1, "Peter", "Tajiri", "Mika",
                "43527515", "0000000", "123"));
    }
}
