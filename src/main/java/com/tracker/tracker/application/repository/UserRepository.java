package com.tracker.tracker.application.repository;

import com.tracker.tracker.application.document.Users;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<Users, Integer> {
}
