package com.tracker.tracker.application.controller;

import com.tracker.tracker.application.document.Users;
import com.tracker.tracker.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserRepository userRepository;


    @GetMapping("/")
    public List<Users> getAll(){
        return userRepository.findAll();
    }
}
